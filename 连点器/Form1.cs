﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading; //导入命名空间,类Thread就在此空间中
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace 连点器
{
    public partial class Form1 : MetroForm
    {
    public KeyboardHookLib _keyboardHook = null;
    public static Keys Cmpkey ;
    public static int ClickOnSleep = 100;
        public Form1()
        {
            InitializeComponent();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            //安装勾子
            _keyboardHook = new KeyboardHookLib();
            _keyboardHook.InstallHook(this.OnKeyPress);
        }
        [System.Runtime.InteropServices.DllImport("user32")]
        private static extern int mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);
        //移动鼠标 
        const int MOUSEEVENTF_MOVE = 0x0001;
        //模拟鼠标左键按下 
        const int MOUSEEVENTF_LEFTDOWN = 0x0002;
        //模拟鼠标左键抬起 
        const int MOUSEEVENTF_LEFTUP = 0x0004;
        //模拟鼠标右键按下 
        const int MOUSEEVENTF_RIGHTDOWN = 0x0008;
        //模拟鼠标右键抬起 
        const int MOUSEEVENTF_RIGHTUP = 0x0010;
        //模拟鼠标中键按下 
        const int MOUSEEVENTF_MIDDLEDOWN = 0x0020;
        //模拟鼠标中键抬起 
        const int MOUSEEVENTF_MIDDLEUP = 0x0040;
        //标示是否采用绝对坐标 
        const int MOUSEEVENTF_ABSOLUTE = 0x8000;
        //模拟鼠标滚轮滚动操作，必须配合dwData参数
        const int MOUSEEVENTF_WHEEL = 0x0800;
        public void OnKeyPress(KeyboardHookLib.HookStruct hookStruct, out bool handle)
        {/*
            textBox1.Text = "";
            //  textBox1.Text = key.ToString();
            string keyName = key.ToString();
            textBox1.Text += (key == Keys.None ? "" : keyName);*/
            Keys key = (Keys)hookStruct.vkCode;
            handle = false; //预设不拦截任何键
            if (key == Cmpkey) 
            {
                Point Mouse_p = new Point();
                Mouse_p = Control.MousePosition;
                mouse_event(MOUSEEVENTF_LEFTDOWN, Mouse_p.X, Mouse_p.Y, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTUP, Mouse_p.X, Mouse_p.Y, 0, 0);
                //Thread.Sleep(ClickOnSleep);
            }
            return;
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            try 
            {
            _keyboardHook.UninstallHook();
            }
            catch (Exception) 
            {
                MessageBox.Show("还没有开启连点 , 停止个锤子\n程序差点就崩了");
            }
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            Set Dlg = new Set();
            Dlg.Show(); 
        }
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            WritePrivateProfileString("Section", "KeyBoard", Form1.Cmpkey.ToString(), ".\\ini\\Info.ini");
            WritePrivateProfileString("Section", "Sleep", Form1.ClickOnSleep.ToString(), ".\\ini\\Info.ini");
            Process.GetCurrentProcess().Kill();
        }
        [System.Runtime.InteropServices.DllImport("kernel32")]
        //private static extern int GetPrivateProfileString(string section, string key, string def, System.Text.StringBuilder retVal, int size, string filePath);
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo _dir = new DirectoryInfo(".\\ini");
            if (!_dir.Exists)
            {
                _dir.Create();
                 FileInfo _f = new FileInfo(".\\ini\\Info.ini");
                using (_f.Create())//释放掉变量
                {

                }
                string strAppFileName = Process.GetCurrentProcess().MainModule.FileName;
                Process myNewProcess = new Process();
                myNewProcess.StartInfo.FileName = strAppFileName;

                myNewProcess.StartInfo.WorkingDirectory = Application.ExecutablePath;
                myNewProcess.Start();
                Application.Exit();
                return;
            }

            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Section","KeyBoard","", temp, 255, ".\\ini\\Info.ini");
            if (temp.ToString() == "") 
            {
                return;
            }
            Cmpkey = (Keys)Enum.Parse(typeof(Keys), temp.ToString());
            GetPrivateProfileString("Section", "Sleep", "", temp, 255, ".\\ini\\Info.ini");
            ClickOnSleep = int.Parse(temp.ToString());
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void 设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Set Dlg = new Set();
            Dlg.Show();

        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Application.Exit();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Visible = false;
                notifyIcon1.Visible = true;
            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.Visible = true;

        }
    }
}
