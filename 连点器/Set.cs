﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 连点器
{
    public partial class Set : MetroForm
    {
    public KeyboardHookLib _keyboardHook = null;
        public Set()
        {
            InitializeComponent();
            //安装勾子
            _keyboardHook = new KeyboardHookLib();
            _keyboardHook.InstallHook(this.OnKeyPress);
        }
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        private void Set_FormClosing(object sender, FormClosingEventArgs e)
        {
            _keyboardHook.UninstallHook();
            Form1.ClickOnSleep = int.Parse(textBox2.Text);
            if (textBox1.Text!="") 
            {
                Form1.Cmpkey= (Keys)Enum.Parse(typeof(Keys),textBox1.Text);
                WritePrivateProfileString("Section", "KeyBoard", Form1.Cmpkey.ToString(), ".\\ini\\Info.ini");
            }

            WritePrivateProfileString("Section", "Sleep", Form1.ClickOnSleep.ToString(), ".\\ini\\Info.ini");
        }

        public void OnKeyPress(KeyboardHookLib.HookStruct hookStruct, out bool handle)
        {
            textBox1.Text = "";
            handle = false; //预设不拦截任何键
            Keys key = (Keys)hookStruct.vkCode;
            //  textBox1.Text = key.ToString();
            string keyName = key.ToString();
            textBox1.Text += (key == Keys.None ? "" : keyName);
            return;
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
